import React, { useEffect, useState } from 'react'
import { Text, StyleSheet, TouchableOpacity,ScrollView, SafeAreaView, View } from 'react-native'
import api from '../config/api'
import ProductCard from '../components/productCard'
import AsyncStorage from '@react-native-community/async-storage'
export default function Products({navigation}) {

    const user = navigation.getParam('user')
    const order = navigation.getParam('order')
    const [products, setProducts] = useState([])
    const [items, setItems] = useState({})

    useEffect(() => {
        const fetchData = async () => {
            
            const results = await api.get('/products')
            setProducts(results.data)
        }

        fetchData()
    }, [])

    const handlerBack = () => {
        navigation.navigate('Main', {user})
    }

    useEffect(() => {
        const logg = () => console.log('OII', items);
        logg()
    }, [items])

    const handlerAddProduct = async (id, key) => {
        const data = {
            amount: items[id],
            price: products[key].price,
            product_id: products[key].id,
            order_id: order,
        }

        const result = await api.post('/order-products', data)
        console.log(result);
    
        if(result){
            delete items[id] 
            setItems({...items})
        }
    }

    const handlerFinishOrder = async () => {
        const result = await api.put(`/orders/${order}`, {status: 'finalizado'})

        await AsyncStorage.removeItem('order')
        navigation.navigate('Main', {user})

    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={handlerBack} style={styles.backButton}>
                    <Text style={styles.buttonText}>Voltar</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={handlerFinishOrder} style={styles.button}>
                    <Text style={styles.buttonText}>Finalizar</Text>
                </TouchableOpacity>
            </View>
            <ScrollView style={{flex: 1, alignSelf: 'stretch'}}>
                <View style={styles.cardsContainer}>
                    {
                        products.map( (product, key) => (
                            <ProductCard product={product} items={items} setItems={setItems} numberKey={key} addProduct={handlerAddProduct} />
                        ))
                    }
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    container: {
        alignSelf: 'stretch',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: '#fcf7e5',
        padding: 30,
    },

    cardsContainer: {
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center',
    },
    
    button: {
        height: 40,
        backgroundColor: '#199c7b',
        borderRadius: 5,
        marginTop: 5,
        marginBottom: 15,
        width: 100,
        marginHorizontal: 10,
        justifyContent: 'space-around',
        alignItems: 'center'
      },

    backButton: {
        height: 40,
        backgroundColor: '#ff0000',
        borderRadius: 5,
        marginTop: 5,
        marginBottom: 5,
        width: 100,
        marginHorizontal: 10,
        justifyContent: 'space-around',
        alignItems: 'center'
      },
    
      buttonText: {
          color: '#FFF',
          fontWeight: 'bold',
          fontSize: 16
      }   
})