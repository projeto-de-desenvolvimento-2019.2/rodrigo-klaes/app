import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

export default function OrderCard(props) {
    const {order} = props
    
    return (
        <TouchableOpacity key={order.key} style={styles.card}>
            <Text style={styles.code}>{order.code}</Text>
            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                <Text style={styles.status, {color: order.status == 'finalizado' ? '#199c7b': '#ff0000'}}>{order.status}</Text>
                <Text style={styles.date}>{order.created_at}</Text>
            </View>
            <Text style={styles.total}>R$ {order.total}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    card: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 10,
        margin: 15,
        overflow: 'hidden',
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        paddingVertical: 15,
        alignSelf: 'stretch',
        elevation: 3
    },

    code:{
        fontSize: 20,
        fontWeight: 'bold',
        color: '#020202',
        textAlign: 'center'
    },

    status: {
        margin: 10,
        fontWeight: 'bold'
    },

    date:{
        margin: 10
    },
    
    total: {
        textAlign: 'center',
        fontSize: 15,
        fontWeight: 'bold'
    }
  
})