import React from 'react'
import {Text, View, TouchableOpacity, StyleSheet, Image, TextInput} from 'react-native'
import plus from '../assets/plus.png'
export default function ProductCard(props) {
    const { product, items, setItems, numberKey, addProduct } = props
    
    return (
        <View key={product.id} style={styles.card}>
            <Text style={styles.name}>{product.name}</Text>
            <Image style={styles.image} source={{ uri: `${product.image}`}}></Image>
            <Text style={styles.code}>Código: {product.code}</Text>
            <Text style={styles.price}>R$ {product.price} - {product.unity}</Text>
            {/* <Text style={styles.category}>{product.category.name || ''}</Text> */}
            <View style={{ justifyContent: 'center', alignItems: 'center'}}>
                <TextInput 
                    style={styles.input}
                    keyboardType='number-pad'
                    maxLength={4}
                    onChangeText={(text) => {
                            items[product.id] = text 
                            setItems({...items})
                        }
                    }
                    value={items[product.id]}
                />
                <TouchableOpacity onPress={() => addProduct(product.id, numberKey)}>
                    <Image source={plus} />
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    card: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 10,
        margin: 15,
        overflow: 'hidden',
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        paddingVertical: 15,
        alignSelf: 'stretch',
        elevation: 3,
        textAlign: 'center',
        alignItems: 'center',
    },

    image: {
        width: 120,
        height: 120,
    },

    input: {
        height: 46,
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#199c7b',
        borderRadius: 5,
        marginTop: 20,
        paddingHorizontal: 15,
        marginBottom: 10
      },

      code: {
          fontWeight: "bold",
      },

      name: {
          fontWeight: 'bold',
          fontSize: 16
      }
})