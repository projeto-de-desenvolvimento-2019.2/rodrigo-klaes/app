# Padoca APP
## Introdução
APP desenvolvido em React Native. 

## Requisitos

- [NodeJs](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/) (Obs: É instalado junto com o NodeJs)
- [React](https://pt-br.reactjs.org/)
- [React Native](https://facebook.github.io/react-native/)

## Link SDK

https://drive.google.com/file/d/1T91AASYKV6fC6RMiVAyCbXSAuPoNPCba/view?usp=sharing

## Configuração

Seguir os passos de  configuração do link abaixo:
https://docs.rocketseat.dev/ambiente-react-native/introducao

## Instalação

```bash
$ npm install
```

## Rodando o app

- Primeiro setar as variáveis de ambiente
- Em seguida, executar um dos comandos abaixo, conforme o ambiente.

```bash
$ react-native run-android

# Ou
$ react-native run-ios

```


- Autor - [Rodrigo Klaes](https://www.linkedin.com/in/klaesrodrigo/)

